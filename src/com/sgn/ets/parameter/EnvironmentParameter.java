package com.sgn.ets.parameter;

import com.konylabs.middleware.common.JavaService2;
import com.konylabs.middleware.controller.DataControllerRequest;
import com.konylabs.middleware.controller.DataControllerResponse;
import com.konylabs.middleware.dataobject.Param;
import com.konylabs.middleware.dataobject.Record;
import com.konylabs.middleware.dataobject.Result;

public class EnvironmentParameter implements JavaService2{

	@Override
	public Object invoke(String arg0, Object[] arg1, DataControllerRequest arg2, DataControllerResponse arg3) throws Exception {
	
	  Result result = new Result();
      
      Record record = new Record();
      record.setID("parameters");
      
      record.setParam(new Param("jdbc_connection",String.valueOf(arg2.getParameter("jdbc_connection")),"String"));
      record.setParam(new Param("db_user",String.valueOf(arg2.getParameter("db_user")),"String"));
      record.setParam(new Param("db_password",String.valueOf(arg2.getParameter("db_password")),"String"));
      
      result.setRecord(record);
     
      return result;
	}


	
}
