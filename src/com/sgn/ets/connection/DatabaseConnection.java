package com.sgn.ets.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {
	
	Connection 	conn 			= null;
	String 		jdbcConnection	= null; 
	String 		dbUser			= null;
	String 		dbPassword		= null;

	public DatabaseConnection(String jdbcConnection, String dbUser, String dbPassword) {
		this.jdbcConnection	= jdbcConnection; 
		this.dbUser			= dbUser;
		this.dbPassword		= dbPassword;
	}
	
	public Connection getConnection() throws ClassNotFoundException, SQLException {
		
		if (conn == null) {
			Class.forName("com.mysql.jdbc.Driver");  
			conn = DriverManager.getConnection(this.jdbcConnection, this.dbUser, this.dbPassword);  			
		}
		
		return conn;
	}
	
	public void closeConnection() throws SQLException {
		this.conn.close();	
		this.conn 	= null;		
	}
	
}
