package com.sgn.ets.integration;


import com.konylabs.middleware.common.JavaService;
import com.konylabs.middleware.common.JavaService2;
import com.konylabs.middleware.controller.DataControllerRequest;
import com.konylabs.middleware.controller.DataControllerResponse;
import com.konylabs.middleware.dataobject.Param;
import com.konylabs.middleware.dataobject.Result;

public class TimeSheetOnline implements JavaService2{

	@Override
	public Object invoke(String arg0, Object[] arg1, DataControllerRequest arg2, DataControllerResponse arg3)
			throws Exception {
		Result rec = new Result();
		rec.setParam(new Param("key1", "val1", "String")); 
		rec.setParam(new Param("key2", "val2", "String")); 
		return rec;
	}

//	@Override
//	public Object invoke(String methodID, Object[] inputArray) throws Exception {
//	// TODO Auto-generated method stub
//	
//		Result rec = new Result();
//		rec.setParam(new Param("key1", "val1", "String")); 
//		rec.setParam(new Param("key2", "val2", "String")); 
//		return rec;
//	}

	
}

