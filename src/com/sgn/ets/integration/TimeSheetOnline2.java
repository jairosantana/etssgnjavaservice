package com.sgn.ets.integration;


import java.util.ArrayList;

import com.konylabs.middleware.common.JavaService2;
import com.konylabs.middleware.controller.DataControllerRequest;
import com.konylabs.middleware.controller.DataControllerResponse;
import com.konylabs.middleware.dataobject.Param;
import com.konylabs.middleware.dataobject.Record;
import com.konylabs.middleware.dataobject.Result;

public class TimeSheetOnline2 implements JavaService2{

	@Override
	public Object invoke(String arg0, Object[] arg1, DataControllerRequest arg2, DataControllerResponse arg3) throws Exception {
	// TODO Auto-generated method stub
	
	  Result result = new Result();
      
      ArrayList<Record> arRecord = new ArrayList<Record>(); 
      
      Record r1 = new Record();
      r1.setID("transaction1");
      r1.setParam(new Param("deliverdate","Oct 01, 2017","String"));
      arRecord.add(r1);
      
      r1 = new Record();
      r1.setID("transaction2");
      r1.setParam(new Param("deliverdate","Oct 02, 2017","String")); 
      arRecord.add(r1);
      
      r1 = new Record();
      r1.setID("transaction3");
      r1.setParam(new Param("deliverdate","Oct 03, 2017","String"));
      arRecord.add(r1);
      
      Record r = new Record();
      r.setID("transactions");
//      r.setParams(paramsList);
      r.setRecords(arRecord);
     
 /*     r.setParam(one);
      r.setParam(two);
      r.setParam(three);
 
      
*/      
      r.setParam(new Param("length",String.valueOf(arRecord.size()),"String"));
      
      
//      r.setParam(new Param("testParam1",arg2.getParameter("testParam1"),"String"));
      
      
      result.setRecord(r);
     
      return result;
	}

	
}

