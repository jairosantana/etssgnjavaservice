package com.sgn.ets.integration.vo;

public class TimesheetTaskVO {
	
	String timesheetID 		= null;
	String timesheetTaskID 	= null;
	String onTime			= null;
	String offTime			= null;
	String day				= null;
	String hrsTypeID			= null;
	String overtimeCodeID	= null;
	String taskID			= null;
	
	public String getTimesheetID() {
		return timesheetID;
	}
	public void setTimesheetID(String timesheetID) {
		this.timesheetID = timesheetID;
	}
	public String getTimesheetTaskID() {
		return timesheetTaskID;
	}
	public void setTimesheetTaskID(String timesheetTaskID) {
		this.timesheetTaskID = timesheetTaskID;
	}
	public String getOnTime() {
		return onTime;
	}
	public void setOnTime(String onTime) {
		this.onTime = onTime;
	}
	public String getOffTime() {
		return offTime;
	}
	public void setOffTime(String offTime) {
		this.offTime = offTime;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getHrsTypeID() {
		return hrsTypeID;
	}
	public void setHrsTypeID(String hrsTypeID) {
		this.hrsTypeID = hrsTypeID;
	}
	public String getOvertimeCodeID() {
		return overtimeCodeID;
	}
	public void setOvertimeCodeID(String overtimeCodeID) {
		this.overtimeCodeID = overtimeCodeID;
	}
	public String getTaskID() {
		return taskID;
	}
	public void setTaskID(String taskID) {
		this.taskID = taskID;
	}

}
