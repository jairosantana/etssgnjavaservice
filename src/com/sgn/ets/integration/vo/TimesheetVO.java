package com.sgn.ets.integration.vo;

public class TimesheetVO {
	String timesheetID 	= null;
	String employeeNo	= null;
	String status		= null;
	public String getTimesheetID() {
		return timesheetID;
	}
	public void setTimesheetID(String timesheetID) {
		this.timesheetID = timesheetID;
	}
	public String getEmployeeNo() {
		return employeeNo;
	}
	public void setEmployeeNo(String employeeNo) {
		this.employeeNo = employeeNo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
