package com.sgn.ets.integration.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.sgn.ets.connection.DatabaseConnection;
import com.sgn.ets.integration.util.Util;
import com.sgn.ets.integration.vo.TimesheetTaskVO;

public class TimesheetTaskDAO {

	private String jdbcConnection	= null;
	private String dbUser			= null;
	private String dbPassword		= null;
	
	public TimesheetTaskDAO(String jdbcConnection, String dbUser, String dbPassword) {
		this.jdbcConnection	= jdbcConnection;
		this.dbUser			= dbUser;
		this.dbPassword		= dbPassword;		
	}
	
	public ArrayList<TimesheetTaskVO> getTimesheetTaskByTSPK(String timesheetID) throws ClassNotFoundException, SQLException {
		ArrayList<TimesheetTaskVO> result = new ArrayList<TimesheetTaskVO>();
		
		if (!Util.stringIsNullEmpty(timesheetID)) {
			DatabaseConnection databaseConnection = new DatabaseConnection(this.jdbcConnection, this.dbUser, this.dbPassword);
			
			String parameters = "";
			String[] arTimesheetID = timesheetID.split(",");
			for (int i=0; i<arTimesheetID.length; i++) {
				if (parameters == "") {
					parameters = "?";					
				}else {
					parameters = parameters + ",?";					
				}
			}
			
			String vSql = "select timesheetTaskID, timesheetID, onTime, offTime, day, hrsTypeID, overtimeCodeID, taskID ";
			vSql = vSql + " from tbltimesheettasks ";
			vSql = vSql + " where timesheetID in (" + parameters + ") ";
			vSql = vSql + " and ISDELETEFLAG is null ";
			vSql = vSql + " order by timesheetID, timesheetTaskID ";
			
			System.out.println(vSql);
			
			PreparedStatement stmt = databaseConnection.getConnection().prepareStatement(vSql);
			
			for (int i=0; i<arTimesheetID.length; i++) {
				stmt.setInt(i+1, Integer.parseInt(arTimesheetID[i]));
			}
			
			ResultSet rs=stmt.executeQuery();  
			
			TimesheetTaskVO timesheetTaskVO = null;
			while(rs.next()) {
				timesheetTaskVO = new TimesheetTaskVO();
				
				timesheetTaskVO.setTimesheetTaskID(rs.getString("timesheetTaskID"));
				timesheetTaskVO.setTimesheetID(rs.getString("timesheetID"));
				timesheetTaskVO.setOnTime(rs.getString("onTime"));
				timesheetTaskVO.setOffTime(rs.getString("offTime"));
				timesheetTaskVO.setDay(rs.getString("day"));
				timesheetTaskVO.setHrsTypeID(rs.getString("hrsTypeID"));
				timesheetTaskVO.setOvertimeCodeID(rs.getString("overtimeCodeID"));
				timesheetTaskVO.setTaskID(rs.getString("taskID"));
				
				result.add(timesheetTaskVO);
			}  
			

			rs.close(); 
			stmt.close();			
			databaseConnection.closeConnection();
			
			rs					= null; 
			stmt					= null;
			databaseConnection	= null;
		}
		
		return result;
	}
	
}
