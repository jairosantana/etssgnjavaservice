package com.sgn.ets.integration.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.sgn.ets.connection.DatabaseConnection;
import com.sgn.ets.integration.util.Util;
import com.sgn.ets.integration.vo.TimesheetVO;

public class TimesheetDAO {
	
	private String jdbcConnection	= null;
	private String dbUser			= null;
	private String dbPassword		= null;
	
	public TimesheetDAO(String jdbcConnection, String dbUser, String dbPassword) {
		this.jdbcConnection	= jdbcConnection;
		this.dbUser			= dbUser;
		this.dbPassword		= dbPassword;		
	}
	
	public ArrayList<TimesheetVO> getTimesheetsByPK(String timesheetID) throws ClassNotFoundException, SQLException {
		ArrayList<TimesheetVO> result = new ArrayList<TimesheetVO>();
		
		if (!Util.stringIsNullEmpty(timesheetID)) {
			DatabaseConnection databaseConnection = new DatabaseConnection(this.jdbcConnection, this.dbUser, this.dbPassword);
			
			String parameters = "";
			String[] arTimesheetID = timesheetID.split(",");
			for (int i=0; i<arTimesheetID.length; i++) {
				if (parameters == "") {
					parameters = "?";					
				}else {
					parameters = parameters + ",?";					
				}
			}
			
			String vSql = "select timesheetID, employeeNo, status from tbltimesheets ";
			vSql = vSql + "where timesheetID in (" + parameters + ") ";
			
			PreparedStatement stmt = databaseConnection.getConnection().prepareStatement(vSql);
			
			for (int i=0; i<arTimesheetID.length; i++) {
				stmt.setInt(i+1, Integer.parseInt(arTimesheetID[i]));
			}

			
			ResultSet rs=stmt.executeQuery();  
			
			TimesheetVO timesheetVO = null;
			while(rs.next()) {
				timesheetVO = new TimesheetVO();
				
				timesheetVO.setTimesheetID(rs.getString("timesheetID"));
				timesheetVO.setEmployeeNo(rs.getString("employeeNo"));
				timesheetVO.setStatus(rs.getString("status"));
				
				result.add(timesheetVO);
			}  
			
			rs.close(); 
			stmt.close();
			databaseConnection.closeConnection();

			rs					= null; 
			stmt					= null;
			databaseConnection	= null;
		}
		
		return result;
	}
}
