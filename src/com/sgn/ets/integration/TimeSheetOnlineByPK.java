package com.sgn.ets.integration;

import java.util.ArrayList;

import com.konylabs.middleware.common.JavaService2;
import com.konylabs.middleware.controller.DataControllerRequest;
import com.konylabs.middleware.controller.DataControllerResponse;
import com.konylabs.middleware.dataobject.Param;
import com.konylabs.middleware.dataobject.Record;
import com.konylabs.middleware.dataobject.Result;
import com.sgn.ets.integration.dao.TimesheetDAO;
import com.sgn.ets.integration.vo.TimesheetVO;

public class TimeSheetOnlineByPK implements JavaService2{
	@Override
	public Object invoke(String arg0, Object[] arg1, DataControllerRequest arg2, DataControllerResponse arg3) throws Exception {
		
		TimesheetDAO timesheetDAO = new TimesheetDAO(arg2.getParameter("jdbc_connection"), arg2.getParameter("db_user"), arg2.getParameter("db_password"));
		
		System.out.println("timesheets:"+ arg2.getParameter("timesheets"));
		
		Result result = arTimeshhetVOToResult(timesheetDAO.getTimesheetsByPK(arg2.getParameter("timesheets")));
     
		return result;
	}
	
	public Result arTimeshhetVOToResult(ArrayList<TimesheetVO> arTimesheetVO){
		Result result = new Result();
		
	      ArrayList<Record> arRecord = new ArrayList<Record>(); 
	      
	      Record recordTimesheet = null;
		
	      TimesheetVO timesheetVO  = null;
		for (int i=0; i<arTimesheetVO.size(); i++){
			timesheetVO = arTimesheetVO.get(i);
			
			recordTimesheet = new Record();
			recordTimesheet.setID("record" + (i+1));
			recordTimesheet.setParam(new Param("timesheetID",timesheetVO.getTimesheetID(),"String"));
			recordTimesheet.setParam(new Param("employeeNo",timesheetVO.getEmployeeNo(),"String"));
			recordTimesheet.setParam(new Param("status",timesheetVO.getStatus(),"String"));
		    arRecord.add(recordTimesheet);
		}

		Record recordTransactions = new Record();
		recordTransactions.setID("records");
		recordTransactions.setRecords(arRecord);
		recordTransactions.setParam(new Param("length",String.valueOf(arRecord.size()),"String"));
		  
		result.setRecord(recordTransactions);

		return result;
	}

}
