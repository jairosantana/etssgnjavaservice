package com.sgn.ets.integration.util;

import java.util.ArrayList;

import com.konylabs.middleware.dataobject.Param;
import com.konylabs.middleware.dataobject.Record;
import com.konylabs.middleware.dataobject.Result;
import com.sgn.ets.integration.vo.TimesheetVO;

public class Util {

	public static boolean stringIsNullEmpty(String pString) {
		boolean result = true;
		
		if ((pString != null) && (pString != "")) {
			result = false;
		}
		
		return result;
	}
		
}
