package com.sgn.ets.integration.test;

import java.util.HashMap;

import com.konylabs.middleware.controller.DataControllerRequest;
import com.konylabs.middleware.controller.DataControllerRequestFactory;
import com.konylabs.middleware.dataobject.Record;
import com.konylabs.middleware.dataobject.Result;
import com.sgn.ets.integration.TimesheetTaskOnlineByTSPK;

public class TimesheetTaskByTSPKTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TimesheetTaskOnlineByTSPK timeSheetTaskOnlineByTSPK = new TimesheetTaskOnlineByTSPK();
		try {
			
			HashMap<String, Object> hashMap = new HashMap<String, Object>();
			hashMap.put("timesheets", "327,328,329");
			hashMap.put("jdbc_connection", "jdbc:mysql://35.176.224.75:3306/etimesheet_uat");
			hashMap.put("db_user", "sgn");
			hashMap.put("db_password", "admin");
			
			DataControllerRequest arg2 = DataControllerRequestFactory.newInstance(hashMap);
			
			Result result = (Result)timeSheetTaskOnlineByTSPK.invoke(null, null, arg2, null);
			
			System.out.println(result.getRecordById("records").getRecords().size());
			
			for (Record record : result.getRecordById("records").getRecords()) {
				System.out.println("timesheetTaskID: " + record.getParam("timesheetTaskID").getValue());
				System.out.println("timesheetID: " + record.getParam("timesheetID").getValue());
				System.out.println("onTime: " + record.getParam("onTime").getValue());
				System.out.println("offTime: " + record.getParam("offTime").getValue());
				System.out.println("day: " + record.getParam("day").getValue());
				System.out.println("hrsTypeID: " + record.getParam("hrsTypeID").getValue());
				System.out.println("overtimeCodeID: " + record.getParam("overtimeCodeID").getValue());
				System.out.println("taskID: " + record.getParam("taskID").getValue());

				System.out.println(" ");
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
