package com.sgn.ets.integration.test;

import java.util.HashMap;

import com.konylabs.middleware.controller.DataControllerRequest;
import com.konylabs.middleware.controller.DataControllerRequestFactory;
import com.konylabs.middleware.dataobject.Result;
import com.sgn.ets.parameter.EnvironmentParameter;

public class TestParameter {

	public static void main(String[] args) {
		EnvironmentParameter environmentParameter = new EnvironmentParameter();
		try {
			
			HashMap<String, Object> hashMap = new HashMap<String, Object>();
			hashMap.put("jdbc_connection", "jdbc:mysql://35.176.224.75:3306/etimesheet_uat");
			hashMap.put("db_user", "sgn");
			hashMap.put("db_password", "admin");

			DataControllerRequest arg2 = DataControllerRequestFactory.newInstance(hashMap);
			
			Result result = (Result)environmentParameter.invoke(null, null, arg2, null);

			System.out.println("jdbc_connection: " + result.getRecordById("parameters").getParam("jdbc_connection").getValue());
			System.out.println("db_user: " + result.getRecordById("parameters").getParam("db_user").getValue());
			System.out.println("db_password: " + result.getRecordById("parameters").getParam("db_password").getValue());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
