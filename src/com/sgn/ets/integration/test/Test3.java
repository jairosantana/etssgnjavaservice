package com.sgn.ets.integration.test;

import java.util.HashMap;

import com.konylabs.middleware.controller.DataControllerRequest;
import com.konylabs.middleware.controller.DataControllerRequestFactory;
import com.konylabs.middleware.dataobject.Record;
import com.konylabs.middleware.dataobject.Result;
import com.sgn.ets.integration.TimeSheetOnlineByPK;

public class Test3 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TimeSheetOnlineByPK timeSheetOnlineByPK = new TimeSheetOnlineByPK();
		try {
			
			HashMap<String, Object> hashMap = new HashMap<String, Object>();
			hashMap.put("timesheets", "327,328,329");
			hashMap.put("jdbc_connection", "jdbc:mysql://35.176.224.75:3306/etimesheet_uat");
			hashMap.put("db_user", "sgn");
			hashMap.put("db_password", "admin");
			
			
			DataControllerRequest arg2 = DataControllerRequestFactory.newInstance(hashMap);
			
			Result result = (Result)timeSheetOnlineByPK.invoke(null, null, arg2, null);
			
			System.out.println(result.getRecordById("records").getRecords().size());
			
			for (Record record : result.getRecordById("records").getRecords()) {
				System.out.println("timesheetID: " + record.getParam("timesheetID").getValue());
				System.out.println("employeeNo: " + record.getParam("employeeNo").getValue());
				System.out.println("status: " + record.getParam("status").getValue());
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
