package com.sgn.ets.integration;

import java.util.ArrayList;

import com.konylabs.middleware.common.JavaService2;
import com.konylabs.middleware.controller.DataControllerRequest;
import com.konylabs.middleware.controller.DataControllerResponse;
import com.konylabs.middleware.dataobject.Param;
import com.konylabs.middleware.dataobject.Record;
import com.konylabs.middleware.dataobject.Result;
import com.sgn.ets.integration.dao.TimesheetTaskDAO;
import com.sgn.ets.integration.vo.TimesheetTaskVO;

public class TimesheetTaskOnlineByTSPK implements JavaService2 {

	@Override
	public Object invoke(String arg0, Object[] arg1, DataControllerRequest arg2, DataControllerResponse arg3) throws Exception {
		
		TimesheetTaskDAO timesheetTaskDAO = new TimesheetTaskDAO(arg2.getParameter("jdbc_connection"), arg2.getParameter("db_user"), arg2.getParameter("db_password"));
		
		System.out.println("timesheets:"+ arg2.getParameter("timesheets"));
		
		Result result = arTimeshhetTaskVOToResult(timesheetTaskDAO.getTimesheetTaskByTSPK(arg2.getParameter("timesheets")));
     
		return result;
	}
	
	public Result arTimeshhetTaskVOToResult(ArrayList<TimesheetTaskVO> arTimesheetTaskVO){
		Result result = new Result();
		
	      ArrayList<Record> arRecord = new ArrayList<Record>(); 
	      
	      Record recordTimesheet = null;
		
	      TimesheetTaskVO timesheetTaskVO  = null;
		for (int i=0; i<arTimesheetTaskVO.size(); i++){
			timesheetTaskVO = arTimesheetTaskVO.get(i);
			
			recordTimesheet = new Record();
			recordTimesheet.setID("record" + (i+1));
			recordTimesheet.setParam(new Param("timesheetTaskID",timesheetTaskVO.getTimesheetTaskID(),"String"));
			recordTimesheet.setParam(new Param("timesheetID",timesheetTaskVO.getTimesheetID(),"String"));
			recordTimesheet.setParam(new Param("onTime",timesheetTaskVO.getOnTime(),"String"));
			recordTimesheet.setParam(new Param("offTime",timesheetTaskVO.getOffTime(),"String"));
			recordTimesheet.setParam(new Param("day",timesheetTaskVO.getDay(),"String"));
			recordTimesheet.setParam(new Param("hrsTypeID",timesheetTaskVO.getHrsTypeID(),"String"));
			recordTimesheet.setParam(new Param("overtimeCodeID",timesheetTaskVO.getOvertimeCodeID(),"String"));
			recordTimesheet.setParam(new Param("taskID",timesheetTaskVO.getTaskID(),"String"));
						
		    arRecord.add(recordTimesheet);
		}

		Record recordTransactions = new Record();
		recordTransactions.setID("records");
		recordTransactions.setRecords(arRecord);
		recordTransactions.setParam(new Param("length",String.valueOf(arRecord.size()),"String"));
		  
		result.setRecord(recordTransactions);

		return result;
	}
	
}
